import chalk from 'chalk';

const NODE_ENV: string = (process.env.NODE_ENV || 'production').toLowerCase();

/**
 * Returns current time in format HH:MM:SS.
 */
function currentTime() {
  const time = new Date();

  return `${time.toISOString().substr(11, 8)}`;
}

/**
 * Prints a message to log output with prefix LOGG and current time.
 *
 * @param message
 */
export function log(message: string): void {
  console.log(
    `${chalk.black('[')}${chalk.yellow('LOGG')}${chalk.black(
      ']',
    )} ${chalk.black.bold(currentTime())} | ${chalk.black(message)}`,
  );
}

/**
 * Prints a message to log output with prefix INFO and current time.
 *
 * @param message
 */
export function info(message: string): void {
  console.log(
    `${chalk.black('[')}${chalk.yellow('INFO')}${chalk.black(
      ']',
    )} ${chalk.black.bold(currentTime())} | ${chalk.black(message)}`,
  );
}

/**
 * Prints a message to log output with prefix DBUG and current time, but
 * if debug disable nothing will happens.
 *
 * @param message
 */
export function debug(message: string): void {
  if (NODE_ENV === 'production') return;

  console.log(
    `${chalk.black('[')}${chalk.green('DBUG')}${chalk.black(
      ']',
    )} ${chalk.black.bold(currentTime())} | ${chalk.black(message)}`,
  );
}

/**
 * Prints a message to log output with prefix WARN and current time.
 *
 * @param message
 */
export function error(message: string): void {
  console.log(
    `${chalk.black('[')}${chalk.red('ERRO')}${chalk.black(
      ']',
    )} ${chalk.black.bold(currentTime())} | ${chalk.black(message)}`,
  );
}

/**
 * Prints a message to log output with prefix ERRO and current time.
 *
 * @param message
 */
export function warning(message: string): void {
  console.log(
    `${chalk.black('[')}${chalk.green('WARN')}${chalk.black(
      ']',
    )} ${chalk.black.bold(currentTime())} | ${chalk.black(message)}`,
  );
}
