import {default as PatrickInstance} from './core';

export * from './core';
export * from './log';
export * from './messaging';
export * from './services';

export default PatrickInstance;
