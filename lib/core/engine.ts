import {createHandyClient, IHandyRedis} from 'handy-redis';
import {IDIConfigurationOptions, ServerLoader} from '@tsed/common';

import {info} from '../log';
import {MessagingManager, MessagingManagerInterface} from '../messaging';
import {
  Service,
  ServiceDataInterface,
  ServiceManager,
  ServiceManagerInterface,
} from '../services';

import {
  ConfigInterface,
  EngineInterface,
  ExpressServer,
  PackageType,
  ServiceStateType,
} from '.';

/**
 * Patrick is a library for sending requests and communication using
 * Redis PUB/SUB between services.
 *
 * This library is built on a partial basis of microservice architecture,
 * but is not full implementation.
 *
 * Patrick Instance allows you to work with the entire system, namely to receive
 * data from the service, perform HTTP requests and just send messages one way.
 *
 * The service is a limited functionality with which anyone can interact
 * another instance of patrick.
 */
export class PatrickEngine implements EngineInterface {
  private config: ConfigInterface | null = null;

  private redis: IHandyRedis | null = null;

  private server: ServerLoader | null = null;

  private readonly serviceManager: ServiceManager = new ServiceManager(this);

  private readonly messagingManager: MessagingManager = new MessagingManager(
    this,
  );

  /**
   * Initializes current Patrick instance and creating connection to redis server.
   *
   * @throws Error if Patrick already initialized.
   * @param config an object of connections params
   */
  public async init(config: ConfigInterface): Promise<void> {
    if (this.config !== null) throw new Error('Patrick already initialized!');

    info('Initializing Patrick...');

    this.config = config;

    this.redis = createHandyClient({
      ...config,

      prefix: 'patrick:',
    });

    info('Connection to Redis initialized successfully.');

    // todo: i don't like it at all
    return new Promise(done => setTimeout(done, 200));
  }

  /**
   * Initializes express module.
   */
  public async initExpressServer(
    settings: Partial<IDIConfigurationOptions> = {},
  ): Promise<ExpressServer> {
    this.server = await ServerLoader.bootstrap(ExpressServer, settings);

    return this.server as ExpressServer;
  }

  /**
   * Initializes current Patrick instance as Service.
   *
   * The Patrick will periodically receive data from the redis server and update it.
   * Based on this scheme, any service can update any other service.
   *
   * Also subscribes to channel's by service type and name to receive data
   * from another patrick instances.
   *
   * @param config config
   *
   * @throws Error if Patrick instance already initialized as service.
   */
  public async initAsService(config: ServiceDataInterface): Promise<void> {
    if (!this.isConnectedToRedis())
      throw new Error("Patrick isn't initialized!");

    if (this.serviceManager.isCurrentService())
      throw new Error('Patrick instance already initialized as service!');

    const routes = [];

    if (this.server) {
      this.server.settings.httpPort = config.port;

      routes.push(...this.getExpressServer().getRoutes());
    }

    const service = new Service({
      routes,

      ...config,
    });

    await this.serviceManager.initializeService(service);
    await this.messagingManager.initializeService(service);

    await this.messagingManager.broadcast(PackageType.SERVICE_STATE, {
      state: ServiceStateType.START,
      name: service.getName(),
      type: service.getType(),
    });

    info('Current Patrick initialized as service successfully.');
    info(
      `Current service ID: ${(await this.getServiceManager().getCurrentService()).getInstanceID()}`,
    );
  }

  /**
   * Gracefully shutdown patrick instance, service and messaging channel.
   */
  public async shutdown(): Promise<void> {
    this.config = null;

    await this.getServiceManager().stopService();
    await this.getMessagingManager().stopSubscriber();

    this.getRedis().redis.end(true);
    this.redis = null;
  }

  /**
   * Returns an instance of {@class IHandyRedis}.
   */
  public getRedis(): IHandyRedis {
    if (!this.isConnectedToRedis())
      throw new Error("Patrick isn't initialized!");

    return this.redis as IHandyRedis;
  }

  /**
   * Returns true, if connected to redis.
   */
  public isConnectedToRedis(): boolean {
    return this.redis !== null && this.redis.redis.connected;
  }

  /**
   * Returns an instance of {@class ServiceManagerInterface}.
   */
  public getServiceManager(): ServiceManagerInterface {
    return this.serviceManager;
  }

  /**
   * Returns an instance of {@class MessagingManagerInterface}.
   */
  public getMessagingManager(): MessagingManagerInterface {
    return this.messagingManager;
  }

  /**
   * Returns an instance on {@class ExpressServer}
   */
  public getExpressServer(): ExpressServer | undefined {
    return this.server as ExpressServer | undefined;
  }

  /**
   * Returns config with redis connection params.
   */
  public getConfig(): ConfigInterface {
    if (!this.isConnectedToRedis())
      throw new Error("Patrick isn't initialized!");

    return this.config;
  }
}
