export enum ServiceStateType {
  START = 'SERVICE_START',

  STOP = 'SERVICE_STOP',
}
