import {RouteService, ServerLoader, ServerSettings} from '@tsed/common';
import chalk from 'chalk';
import * as bodyParser from 'body-parser';
import * as compression from 'compression';
import * as cors from 'cors';

import {ServiceRouteInterface} from '..';

import {PatrickRouteConfig} from '.';

@ServerSettings({
  acceptMimes: ['application/json'],
  logger: {
    disableRoutesSummary: true,
    format: `${chalk.black('[')}${chalk.yellow('%p%]')}${chalk.black(
      ']',
    )} ${chalk.black('%d{hh:mm:ss} | %m')}`,
  },
  httpsPort: false,
})
export class ExpressServer extends ServerLoader {
  private patrickRoutes: ServiceRouteInterface[] = [];

  // noinspection JSUnusedGlobalSymbols
  public $beforeRoutesInit(): void | Promise<any> {
    this.use(cors())
      .use(compression())
      .use(bodyParser.json())
      .use(
        bodyParser.urlencoded({
          extended: true,
        }),
      );
  }

  // noinspection JSUnusedGlobalSymbols
  public $afterRoutesInit(): void | Promise<any> {
    const service = this.injector.get<RouteService>(RouteService);

    service.routes.forEach(controller => {
      const controllerRoute = controller.route;

      controller.provider.endpoints.forEach(route => {
        const data: PatrickRouteConfig = route.store.get('route');

        if (data !== null && data !== undefined) {
          route.pathsMethods.forEach(method => {
            this.patrickRoutes.push({
              route: controllerRoute + method.path,
              method: method.method,

              ...data,
            });
          });
        }
      });
    });
  }

  public getRoutes(): ServiceRouteInterface[] {
    return this.patrickRoutes;
  }
}
