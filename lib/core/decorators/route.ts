import {Store, Type} from '@tsed/core';

export interface PatrickRouteConfig {
  external: boolean;

  [key: string]: any;
}

/**
 * Use that function to annotate http route for api-gateway handling.
 *
 * @param route
 */
export function PatrickRoute(
  route: PatrickRouteConfig = {external: false},
): Function {
  return <T>(
    target: Type<any>,
    targetKey?: string,
  ): TypedPropertyDescriptor<T> | void => {
    Store.fromMethod(target, targetKey).set('route', route);
  };
}
