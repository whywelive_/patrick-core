import {MessageContentInterface} from '../../messaging';

import {ServiceStateType} from '..';

export interface ServiceStateInterface extends MessageContentInterface {
  state: ServiceStateType;
  name: string;
  type: string;
}
