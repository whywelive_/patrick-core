import {IDIConfigurationOptions, ServerLoader} from '@tsed/common';

import {MessagingManagerInterface} from '../../messaging';
import {ServiceDataInterface, ServiceManagerInterface} from '../../services';

import {ConfigInterface, ExpressServer} from '..';

/**
 * Interface of methods to work with Patrick.
 */
export interface EngineInterface {
  /**
   * Initializes current Patrick instance and creating connection to redis server.
   *
   * @throws Error if Patrick already initialized.
   * @param config an object of connections params
   */
  init(config: ConfigInterface): Promise<void>;

  /**
   * Initializes express module.
   */
  initExpressServer(
    settings: Partial<IDIConfigurationOptions>,
  ): Promise<ServerLoader>;

  /**
   * Initializes current Patrick instance as Service.
   *
   * The Patrick will periodically receive data from the redis server and update it.
   * Based on this scheme, any service can update any other service.
   *
   * Also subscribes to channel's by service type and name to receive data
   * from another patrick instances.
   *
   * @param config config
   *
   * @throws Error if Patrick instance already initialized as service.
   */
  initAsService(config: ServiceDataInterface): Promise<void>;

  /**
   * Gracefully shutdown patrick instance, service and messaging channel.
   */
  shutdown(): Promise<void>;

  /**
   * Returns true, if connected to redis.
   */
  isConnectedToRedis(): boolean;

  /**
   * Returns an instance of {@class ServiceManagerInterface}.
   */
  getServiceManager(): ServiceManagerInterface;

  /**
   * Returns an instance of {@class MessagingManagerInterface}.
   */
  getMessagingManager(): MessagingManagerInterface;

  /**
   * Returns an instance on {@class ExpressServer}
   */
  getExpressServer(): ExpressServer | null;
}
