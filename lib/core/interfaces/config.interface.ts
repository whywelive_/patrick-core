/**
 * A Configuration object containing the configuration entries to create connection with redis server.
 */
export interface ConfigInterface {
  /**
   * Hostname.
   */
  readonly host?: string;

  /**
   * Port.
   */
  readonly port?: number;

  /**
   * Password.
   */
  readonly password?: string;

  /**
   * Database number.
   */
  readonly db?: string | number;
}
