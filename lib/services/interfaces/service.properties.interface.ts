export interface ServicePropertiesInterface {
  /**
   * Properties
   */
  [key: string]: any;
}
