import {ServiceInterface} from '.';

/**
 * A manager for controlling and monitoring a set of services.
 *
 * This class provides methods for registering, getting and updating services.
 *
 * Each service has its own type, it is used for communication between services.
 *
 * There are three statuses of {@class Service}: available - service successfully initialized
 * and ready for requests, locked - service successfully initialized and ready for requests,
 * but shouldn't be used, down - service not initialized and can't be used.
 *
 * Current {@class Patrick} instance could be a service instance, as well.
 *
 * It retrieves and saves data on redis server.
 */
export interface ServiceManagerInterface {
  /**
   * Returns the service with specified key.
   *
   * @throws Error if service not found.
   * @param key
   */
  getService(key: string): Promise<ServiceInterface>;

  /**
   * Returns the service with specified name.
   *
   * @throws Error if service not found.
   * @param name
   */
  getServiceByName(name: string): Promise<ServiceInterface>;

  /**
   * Returns all services with the given type, even if service status is down.
   *
   * If service type is omitted it will returns all services with any type.
   *
   * @param serviceType
   */
  getAllServices(serviceType?: string): Promise<ServiceInterface[]>;

  /**
   * Returns current service.
   */
  getCurrentService(): ServiceInterface;

  /**
   * Stops current service gracefully.
   */
  stopService(): Promise<void>;

  /**
   * Returns true, if current patrick instance is initialized as service.
   */
  isCurrentService(): boolean;
}
