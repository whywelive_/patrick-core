import {ServiceRouteInterface, ServicePropertiesInterface} from '.';

export interface ServiceDataInterface {
  /**
   * Unique id of the service.
   */
  instanceID?: string;

  /**
   * Name of the service.
   */
  name: string;

  /**
   * Type of the service.
   */
  type: string;

  /**
   * Version of the service.
   */
  version: string;

  /**
   * Host of the service.
   */
  host: string;

  /**
   * Port of the service.
   */
  port: number;

  /**
   * API-Route for gateway.
   */
  route: string;

  /**
   * Date when the service was registered.
   */
  registeredOn?: number;

  /**
   * Date when the service was updated.
   */
  updatedOn?: number;

  /**
   * Service's routes.
   */
  routes?: ServiceRouteInterface[];

  /**
   * Service's properties.
   */
  properties?: ServicePropertiesInterface;
}
