import {
  MessageContentInterface,
  RequestInterface,
  ResultInterface,
} from '../../messaging';

import {ServiceRouteInterface, ServicePropertiesInterface} from '.';

/**
 * Interface of methods to work with Patrick.
 */
export interface ServiceInterface {
  /**
   * Performs an HTTP request to the specified route.
   *
   * @param request
   */
  makeAPIRequest<T>(request: RequestInterface): Promise<ResultInterface<T>>;

  /**
   * Sends a message to the service using Redis PUB/SUB.
   */
  sendMessage(
    packageType: string,
    data: MessageContentInterface,
  ): Promise<void>;

  /**
   * Returns the unique id of the service.
   */
  getInstanceID(): string;

  /**
   * Returns the name of the service.
   */
  getName(): string;

  /**
   * Returns the type of the service.
   */
  getType(): string;

  /**
   * Returns the version of the service.
   */
  getVersion(): string;

  /**
   * Returns the host of the service.
   */
  getHost(): string;

  /**
   * Returns the port of the service.
   */
  getPort(): number;

  /**
   * Returns the api route of the service.
   */
  getAPIRoute(): string;

  /**
   * Returns the date when the service was registered.
   */
  getRegisteredOn(): number;

  /**
   * Returns the date when the service was last updated.
   */
  getUpdatedOn(): number;

  /**
   * Routes, that service can handle.
   */
  getRoutes(): ServiceRouteInterface[];

  /**
   * Returns an object of properties.
   */
  getProperties(): ServicePropertiesInterface;
}
