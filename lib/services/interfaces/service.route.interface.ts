export interface ServiceRouteInterface {
  method: string;
  route: string;
  external: boolean;

  [key: string]: any;
}
