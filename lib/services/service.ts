import {v4 as uuid} from 'uuid';
import axios, {Method} from 'axios';

import {default as Patrick} from '../core';
import {debug} from '../log';

import {
  MessageContentInterface,
  MessageInterface,
  MessageSourceInterface,
  RequestInterface,
  ResultInterface,
} from '../messaging';

import {
  ServiceInterface,
  ServiceDataInterface,
  ServicePropertiesInterface,
  ServiceRouteInterface,
} from '.';

export class Service implements ServiceInterface {
  private readonly instanceID: string;

  private readonly name: string;

  private readonly type: string;

  private readonly version: string;

  private readonly host: string;

  private readonly port: number;

  private readonly route: string;

  private readonly registeredOn: number = 0;

  private readonly updatedOn: number = 0;

  private readonly routes: ServiceRouteInterface[];

  private readonly properties: ServicePropertiesInterface = {};

  /**
   * @param config
   */
  public constructor(config: ServiceDataInterface) {
    if (config.name.length === 0) throw Error('Service name cannot be empty!');
    if (config.type.length === 0) throw Error('Service type cannot be empty!');
    if (config.version.length === 0)
      throw Error('Service version cannot be empty!');

    this.instanceID = config.instanceID || uuid().replace(RegExp('-', 'g'), '');

    this.name = config.name.trim().toUpperCase();
    this.type = config.type.trim().toUpperCase();
    this.version = config.version.trim().toUpperCase();
    this.host = config.host.trim().toLowerCase();
    this.port = config.port;
    this.route = config.route.toLowerCase();
    this.routes = config.routes || [];
    this.properties = config.properties || {};

    this.registeredOn = config.registeredOn || new Date().getTime();
    this.updatedOn = config.updatedOn || new Date().getTime();
  }

  /**
   * Performs an HTTP request to the specified route.
   *
   * @param request
   */
  public async makeAPIRequest<T>(
    request: RequestInterface,
  ): Promise<ResultInterface<T>> {
    const data = request.target.split('|');
    const method = data[0] as Method;
    const route = data[1];

    debug(
      `Performing REQUEST ${method} || ${route} to ${this.getHost()}:${this.getPort()}`,
    );

    try {
      const result = await axios({
        method,
        url: `http://${this.getHost()}:${this.getPort()}/${route}`,

        ...request,
      });

      return {
        success: true,
        httpCode: result.status,
        data: result.data.data,
      };
    } catch (e) {
      return {
        success: false,
        httpCode: 400,
      };
    }
  }

  /**
   * Sends a message to the service using Redis PUB/SUB.
   *
   * @param packageType
   * @param data
   */
  public async sendMessage(
    packageType: string,
    data: MessageContentInterface,
  ): Promise<void> {
    let sourceName = '';
    let sourceType = '';

    if (Patrick.getServiceManager().isCurrentService()) {
      const service = await Patrick.getServiceManager().getCurrentService();

      sourceName = service.getName();
      sourceType = service.getType();
    }

    const source: MessageSourceInterface = {
      name: sourceName,
      type: sourceType,
    };

    const message: MessageInterface<MessageContentInterface> = {
      packageType,
      source,
      data,
    };

    await Patrick.getRedis().publish(
      `patrick:messaging:service:${this.getType()}:${this.getName()}`,
      JSON.stringify(message),
    );
  }

  /**
   * Returns the unique id of the service.
   */
  public getInstanceID(): string {
    return this.instanceID;
  }

  /**
   * Returns the name of the service.
   */
  public getName(): string {
    return this.name;
  }

  /**
   * Returns the type of the service.
   */
  public getType(): string {
    return this.type;
  }

  /**
   * Returns the version of the service.
   */
  public getVersion(): string {
    return this.version;
  }

  /**
   * Returns the host of the service.
   */
  public getHost(): string {
    return this.host;
  }

  /**
   * Returns the port of the service.
   */
  public getPort(): number {
    return this.port;
  }

  /**
   * Returns the api route of the service.
   */
  public getAPIRoute(): string {
    return this.route;
  }

  /**
   * Returns the date when the service was registered.
   */
  public getRegisteredOn(): number {
    return this.registeredOn;
  }

  /**
   * Returns the date when the service was last updated.
   */
  public getUpdatedOn(): number {
    return this.updatedOn;
  }

  /**
   * Returns a collection of available routes that
   * the service can handle.
   */
  public getRoutes(): ServiceRouteInterface[] {
    return this.routes;
  }

  /**
   * Returns an object of properties.
   */
  public getProperties(): ServicePropertiesInterface {
    return this.properties;
  }
}
