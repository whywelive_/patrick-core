import {
  PatrickEngine,
  PackageType,
  ServiceStateType,
  default as Patrick,
} from '../core';

import {ServiceManagerInterface, Service} from '.';

/**
 * A manager for controlling and monitoring a set of services.
 *
 * This class provides methods for registering, getting and updating services.
 *
 * Each service has its own type, it is used for communication between services.
 *
 * There are three statuses of {@class Service}: available - service successfully initialized
 * and ready for requests, locked - service successfully initialized and ready for requests,
 * but shouldn't be used, down - service not initialized and can't be used.
 *
 * Current {@class Patrick} instance could be a service instance, as well.
 *
 * It retrieves and saves data on redis server.
 */
export class ServiceManager implements ServiceManagerInterface {
  /**
   * Patrick instance.
   */
  private patrick: PatrickEngine;

  /**
   * Current service instance.
   *
   * -1, if current patrick instance is not initialized as service.
   */
  private service: Service | undefined;

  private updateHealthInterval: any | -1 = -1;

  /**
   * Constructs a new instance to manage services for given patrick instance.
   * @param patrick
   */
  public constructor(patrick: PatrickEngine) {
    this.patrick = patrick;
  }

  /**
   * Returns the service with the specified key.
   * @param key
   */
  public async getService(key: string): Promise<Service> {
    const multi = this.patrick
      .getRedis()
      .multi()
      .get(`${key}:info`)
      .get(`${key}:properties`)
      .get(`${key}:routes`);

    const results = await this.patrick.getRedis().execMulti(multi);

    const serviceInfo = JSON.parse(results[0] as string);
    const serviceProperties = JSON.parse(results[1] as string);
    const serviceRoutes = JSON.parse(results[2] as string);

    return new Service({
      ...serviceInfo,

      routes: serviceRoutes,
      properties: serviceProperties,
    });
  }

  /**
   * Returns service with the given name.
   *
   * @param name
   */
  public async getServiceByName(name: string): Promise<Service> {
    const service = this.getAllServices().then(services =>
      services.find(find => find.getName() === name),
    );

    if (!service) throw new Error('Service with specified name not found!');

    return service;
  }

  /**
   * Returns all services with the given type.
   *
   * @param serviceType
   */
  public async getAllServices(serviceType?: string): Promise<Service[]> {
    const data = (await this.patrick
      .getRedis()
      .keys('patrick:service:*:info')) as string[];

    return Promise.all(
      data.map(key =>
        this.getService(key.replace('patrick:', '').replace(':info', '')),
      ),
    )
      .then(services =>
        services.filter(
          (element: Service, position: number, array: Service[]) =>
            array.indexOf(element) === position,
        ),
      )
      .then(services =>
        services.filter(
          service => !serviceType || service.getType() === serviceType,
        ),
      );
  }

  /**
   * Initializes current patrick instance as service.
   *
   * @param service
   */
  public async initializeService(service: Service): Promise<Service> {
    if (this.isCurrentService())
      throw new Error(
        'Current Patrick instance already initialized as service!',
      );

    this.service = service;

    this.updateService();

    this.updateHealthInterval = setInterval(
      this.updateService.bind(this),
      15 * 1000,
    );

    return service;
  }

  /**
   * Saves service's information to redis.
   */
  private async updateService(): Promise<void> {
    const multi = Patrick.getRedis()
      .multi()
      .setex(
        `service:${this.getCurrentService().getInstanceID()}:info`,
        20,
        JSON.stringify({
          instanceID: this.service.getInstanceID(),
          name: this.service.getName(),
          type: this.service.getType(),
          version: this.service.getVersion(),
          host: this.service.getHost(),
          port: this.service.getPort(),
          route: this.service.getAPIRoute(),
          updatedOn: new Date().getTime(),
          registeredOn: this.service.getRegisteredOn(),
        }),
      )
      .setex(
        `service:${this.service.getInstanceID()}:properties`,
        20,
        JSON.stringify(this.service.getInstanceID()),
      )
      .setex(
        `service:${this.service.getInstanceID()}:routes`,
        20,
        JSON.stringify(this.service.getRoutes()),
      );

    await Patrick.getRedis().execMulti(multi);
  }

  /**
   * Stops current service gracefully.
   */
  public async stopService(): Promise<void> {
    if (!this.isCurrentService()) return;

    const service = await this.getCurrentService();

    await this.patrick
      .getMessagingManager()
      .broadcast(PackageType.SERVICE_STATE, {
        state: ServiceStateType.STOP,
        name: service.getName(),
        type: service.getType(),
      });

    clearInterval(this.updateHealthInterval);

    await this.patrick
      .getRedis()
      .del(`service:${this.getCurrentService().getInstanceID()}`);

    this.updateHealthInterval = -1;
  }

  /**
   * Return current service.
   */
  public getCurrentService(): Service {
    if (!this.isCurrentService())
      throw new Error(
        'Current Patrick instance is not initialized as service!',
      );

    return this.service;
  }

  /**
   * Returns true, if current patrick instance is initialized as service.
   */
  public isCurrentService(): boolean {
    return this.service !== undefined;
  }
}
