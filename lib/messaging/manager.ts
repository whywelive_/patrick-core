import {createHandyClient, IHandyRedis} from 'handy-redis';
import {EventEmitter} from 'events';

import {PatrickEngine} from '../core';
import {debug} from '../log';
import {Service} from '../services';

import {
  MessageContentInterface,
  MessageSourceInterface,
  MessagingManagerInterface,
  RequestInterface,
  ResultInterface,
} from '.';

export class MessagingManager extends EventEmitter
  implements MessagingManagerInterface {
  /**
   * Patrick instance.
   */
  private readonly patrick: PatrickEngine;

  private subscriber: IHandyRedis | null = null;

  /**
   * Constructs a new instance to communicate between services for given patrick instance.
   * @param patrick
   */
  public constructor(patrick: PatrickEngine) {
    super();

    this.patrick = patrick;
  }

  /**
   * Closes redis connection for pub/sub.
   */
  public async stopSubscriber(): Promise<void> {
    if (!this.isSubscriberInitialized()) return;

    this.subscriber.redis.end(true);
    this.subscriber = null;
  }

  /**
   *
   * @param service
   */
  public async initializeService(service: Service): Promise<void> {
    this.subscriber = await createHandyClient({
      ...this.patrick.getConfig(),
    });

    await this.subscriber.subscribe([
      `patrick:messaging:service:${service.getType()}:${service.getName()}`,
    ]);

    this.subscriber.redis.on('message', (channel, message) => {
      const data = channel.split(':');
      const type = data[3];
      const name = data[4];

      const targetSource: MessageSourceInterface = {
        name,
        type,
      };

      debug('Got message:');
      debug(` Source: ${JSON.stringify(targetSource)}`);
      debug(` Content: ${JSON.stringify(JSON.parse(message))}`);

      this.emit('message', targetSource, JSON.parse(message));
    });

    // todo: i don't like it at all
    return new Promise(done => setTimeout(done, 200));
  }

  /**
   * Returns true, if current patrick instance is initialized as service.
   */
  public isSubscriberInitialized(): boolean {
    return this.subscriber != null && this.subscriber.redis.connected;
  }

  /**
   * Performs an HTTP request random service with the given name.
   *
   * @param name
   * @param request
   */
  public async makeAPIRequestByName<T>(
    name: string,
    request: RequestInterface,
  ): Promise<ResultInterface<T>> {
    const service = await this.patrick
      .getServiceManager()
      .getServiceByName(name);

    return service.makeAPIRequest<T>(request);
  }

  /**
   * Performs an HTTP request random service with the given type.
   *
   * @param type
   * @param request
   */
  public async makeAPIRequestByType<T>(
    type: string,
    request: RequestInterface,
  ): Promise<ResultInterface<T>> {
    const service = await this.patrick
      .getServiceManager()
      .getAllServices(type)
      .then(services => services[0]);

    return service.makeAPIRequest<T>(request);
  }

  /**
   * Broadcasts a message to all available services.
   *
   * @param packageType
   * @param message
   *
   * @return amount of services that should receive a message
   */
  public async broadcast(
    packageType: string,
    message: MessageContentInterface,
  ): Promise<number> {
    return this.broadcastByType('', packageType, message);
  }

  /**
   * Broadcasts a message to all available services with the given type.
   *
   * @param type
   * @param packageType
   * @param message
   *
   * @return amount of services that should receive a message
   */
  public async broadcastByType(
    type: string,
    packageType: string,
    message: MessageContentInterface,
  ): Promise<number> {
    if (!this.isSubscriberInitialized())
      throw Error('Redis PUB/SUB client is not initialized');

    const services = await this.patrick
      .getServiceManager()
      .getAllServices(type);

    const operations: Promise<void>[] = [];

    services.forEach(service => {
      operations.push(
        (async (): Promise<void> => {
          await service.sendMessage(packageType, message);
        })(),
      );
    });

    return Promise.all(operations).then(results => results.length);
  }
}
