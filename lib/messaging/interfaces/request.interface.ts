import {AxiosRequestConfig} from 'axios';

export interface RequestInterface extends AxiosRequestConfig {
  /**
   * HTTP point to perform.
   */
  readonly target: string;
}
