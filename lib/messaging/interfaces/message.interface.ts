import {MessageContentInterface, MessageSourceInterface} from '.';

export interface MessageInterface<T extends MessageContentInterface> {
  packageType?: string;
  source: MessageSourceInterface;
  data: T;
}
