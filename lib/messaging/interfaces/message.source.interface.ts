export interface MessageSourceInterface {
  name: string;
  type: string;
}
