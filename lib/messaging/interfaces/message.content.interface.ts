export interface MessageContentInterface {
  [key: string]: any;
}
