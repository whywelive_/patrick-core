import {EventEmitter} from 'events';

import {ServiceStateInterface} from '../../core';

import {
  RequestInterface,
  ResultInterface,
  MessageContentInterface,
  MessageSourceInterface,
} from '.';

export interface MessagingManagerInterface extends EventEmitter {
  /**
   * Closes redis connection for pub/sub.
   */
  stopSubscriber(): Promise<void>;

  /**
   * Returns true, if current patrick instance is initialized as service.
   */
  isSubscriberInitialized(): boolean;

  /**
   * Performs an HTTP request random service with the given type.
   *
   * @param type
   * @param request
   */
  makeAPIRequestByType<T>(
    type: string,
    request: RequestInterface,
  ): Promise<ResultInterface<T>>;

  /**
   * Performs an HTTP request random service with the given name.
   *
   * @param name
   * @param request
   */
  makeAPIRequestByName<T>(
    name: string,
    request: RequestInterface,
  ): Promise<ResultInterface<T>>;

  /**
   * Broadcasts a message to all available services with the given type.
   *
   * @param type
   * @param packageType
   * @param message
   *
   * @return amount of services that should receive a message
   */
  broadcastByType(
    type: string,
    packageType: string,
    message: MessageContentInterface,
  ): Promise<number>;

  /**
   * Broadcasts a message to all available services.
   *
   * @param packageType
   * @param message
   *
   * @return amount of services that should receive a message
   */
  broadcast(
    packageType: string,
    message: MessageContentInterface,
  ): Promise<number>;

  on(
    event: 'message',
    listener: (
      channel: MessageSourceInterface,
      message: ServiceStateInterface,
    ) => void,
  ): this;

  on(
    event: 'message',
    listener: (channel: MessageSourceInterface, message: any) => void,
  ): this;

  on(event: string, listener: (...args: any[]) => void): this;
}
