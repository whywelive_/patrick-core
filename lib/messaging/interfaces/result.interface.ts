export interface ResultInterface<T> {
  success: boolean;
  httpCode: number;
  data?: T;
}
