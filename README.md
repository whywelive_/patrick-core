# Installation

This is [Node.JS](https://nodejs.org/en/) module available through github package registry.
Before installing, [Download and install Node.JS](https://nodejs.org/en/download/). Node.js 0.10 or higher is required.
Also you need to [Configure NPM for use with GitHub Package Registry](https://help.github.com/articles/configuring-npm-for-use-with-github-package-registry/).
Installation is done using the [npm install](https://docs.npmjs.com/downloading-and-installing-packages-locally), if you are using ssh:

```
$ npm install @whywelive/patrick-core@0.3.1
```

However if you are a bad boy you can do it through package.json:

```
"@whywelive/patrick-core": "0.3.1"
```

# Features

- Communication between services via Redis PUB/SUB
- Communication between services via http
- Retrieving information about all services

# Quick starter

```typescript
(async () => {
  await PatrickInstance.init({});

  const server = await PatrickInstance.initExpressServer();

  await PatrickInstance.initAsService(
    new Service({
      name: 'QuickStarter-1',
      type: 'QUICKSTARTER',
      version: '1.0.0.0',
      route: '/example/',
      host: 'localhost',
      port: 3000,
    }),
  );

  await server.listen();
})();
```
